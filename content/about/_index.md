+++
title = "About Me"
template = "about.html"
[extra]
salutation = "Hello!"
+++
My name is Abhinav Anand. Welcome to my page. I am currently pursuing Masters of Technology at Indian Institute of Technology (Hyderabad), India where I work with [Dr. Sumohana S. Channappayya](https://www.iith.ac.in/~sumohana/) as a member of [LFOVIA](https://www.iith.ac.in/~lfovia/) research group.

I graduated from National Institute of Technology (Hamirpur, H.P), India with B.Tech in Electronics and Communications.

Currently, I work on autonomous vehicles and their performance in adverse weather conditions. The work is funded by [TiHAN, IIT Hyderabad](https://tihan.iith.ac.in/).

My research interests include computer vision, trustworthy and robust models, AI safety and their aplications to robotics and autonomous vehicles .

Other than that I am also interested in computer architecture, digital circuit design, type-safe neural networks.

\
\
## What's here?
Nothing Yet. But I am intending to write about
- AI/ML robustness and safety
- Some Details of work I do and publish
- Programming(usually about Rust)
- Random topics I come across and find interesting

\
\
## What else?
I love writing code in Rust and use that to solve Project Euler problems. But I use python for most of my academic work. (I really hope Julia catches up soon so I can use that instead).

I like reading books (suggest something nice you have read recently), sports (football is my favourite), travelling.

I use Linux (Pop! OS) and prefer using open source software over proprietary ones.

\
\
## Contact
If you have any suggestion or want to collaborate on something, please open an isuue on the repo of this website or send me a mail.

(I will probably put up some better means of communication in the future.) 

\
\
## Privacy and other details:
The website doesn't store any cookies and neither uses any analytics tools to track the users that visit the website. Though, I am thinking of implementing a basic, privacy-respecting analytics tool to obtain some non-identifiable data about visitors. If I do that, I will mention that here and write a blog explaing how I implemented that. And I will open source the tool for transparency and others to use.

The website has been rendered with [Zola](https://getzola.org) and uses a modified [ntun](https://www.getzola.org/themes/ntun/) theme.
