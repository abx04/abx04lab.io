+++
title = "Research Summary"
template = "research.html"
+++
My research currently involves the performance of self driving cars in Indian road conditions and in adverse weather conditions. I specifically focus on state-of-the-art object detection models during rain.

The major challenge in obtaining good performance of these models in rain (or other adverse weathers like fog, snow etc.) is primarily a lack of data for those conditions. However, given the dynamic nature of these weather conditions, no amount of data can capture the entire distribution. 

Rain comes with it's own sets of challenges and diveristy. On the one han there are low frequience componets such as water droplets and not the high frequency ones like rain streaks. Further, there is the effect of depth. The performance decreases significantly due to depth as rain accumulates more in front of camera the farther away the object is. Then there are reflections, that get identified as objects.

It's really difficult to capture all this diversity in a dataset. And even if we succeed in doing that, we will have to make a model learn all that, maybe even come up with a new architecture to achieve this with  a lot more parameters. Then, a new adverse condition will arrive or we want to apply our model to a new problem, say off-road vehicles or vehicles in villages, and we are back to collecting data.

Throwing more and more data at the problem won't bring us closer to the solution, at least not a generalized solution. Instead, it is important that we make the current models learn a better representaiton of the world. It wil also help if the model understands it's uncertainities and it's fallacies.

My research interest lies in this direction. For this, I have explored ideas form self-supervised learning, contrastive learning, domain adaptation, transfer learning, knowledge distillation, monocular depth estimation. I have also used with image to image translation models as a tool for data augmentation and explored image deraining models and their effects on various downstream tasks. 

\
\

## Research Intersts
Computer Vision, Robust and Trustworthy Machine Leanring, Robot Vision, Self-driving Vehicles
