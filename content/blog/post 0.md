+++
title = "Post 0"
template = "page.html"
weight = 0
date = 2021-07-26
draft = true
description="Post 0 is the first post. I am just testing stuff and seeing how it works. Well the layout seems kinda weird. Let's see how long this goes. Well..... Finally the texts are wrapping and now it looks good."
+++
**This is the Post 0 to test the settings with bold**
This is the Post 0 to test the settings without bold

```rust, linenos
let mut a = 0;
function print_name(name: &str) {
    println!(name)
}
```
\
Well the code block looks noce now too. I should probably make the text colors a little darker though.

Also i have to create shortccodes for links to references. That would be fun.

But first work on the first blog post.

The code block looks good now.

But how about Julia code?
```julia, linenos
let mut a = 0;
function print_name(name: &str) {
    println!(name)
}
```
\
and python code
```python, linenos
let mut a = 0;
function print_name(name: &str) {
    println!(name)
}
```
\
and inline? ```let mut a = 0```
